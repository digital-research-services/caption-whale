# Caption Whale - Beta

Create closed-captions for videos, using a Docker project that can be deployed to your desktop.

## Instructions

### One-time setup instructions

1. If you don't already have Docker Desktop installed on your computer, install it by starting at the [Docker: Get Started](https://www.docker.com/get-started) page. 

2. Obtain your aws_credentials file and note the location in your filesystem. [This was sent in an email from Keith 2020-10-22, Caption Whale repository location]


### Beta run instructions

1. Copy or move the mp4/m4v files that needed to be captioned into a single directory. Take note of the absolute path of the directory.

2. Run the below command, but: 

- replace MP4_DIR with the absolute path of the directory that you remembered from the previous step.
- replace AWS_CREDENTIALS_PATH with the absolute path to your aws_credentials file

```  docker image pull code.vt.edu:5005/digital-research-services/caption-whale; docker run -it --rm --mount type=bind,source=MP4_DIR,target=/incoming_mp4  --mount type=bind,source=AWS_CREDENTIALS_PATH,target=/root/.aws/credentials code.vt.edu:5005/digital-research-services/caption-whale ```


Note that **the replacements for MP4_DIR and AWS_CREDENTIALS_PATH need to be absolute (not relative) paths**. An example of an absolute path is /Volumes/keithg/video_files. As this is a very long command, you may wish to paste it into a text editor, edit it, and then copy and paste the edited version onto the command line.

This process will take some time, because in this initial version the videos are submitted to be transcribed to Amazon Web Services one at a time instead of simultaneously.

### Editing the resulting .vtt files

Edit the .vtt files that are created in your MP4_DIR with any text editor (BBEdit, vim, etc.) to make necessary corrections. This is
an important step. Even though automatic speech recognition has improved greatly in recent years, it still makes embarrassing and
nonsensical mistakes.


### Tips
 - If you have large videos, perform this process on a computer with a very fast network connection, because the videos must be uploaded to AWS S3 for the transcription process
 - Similarly, it's often best if the videos are on storage local to your computer, instead of network or cloud storage, because the videos will essentially need to be downloaded to your site before they are uploaded to S3.
 - This process works best with videos that have clear, good quality audio. The process can produce extremely poor output when the audio quality is poor, if the speakers have seldom heard accents, or if there are multiple simultaneous speakers
 
### Dependencies and Components

The following are installed and configured automatically during the docker image creation:

- Amazon Linux 2
- Amazon Web Services Command Line Interface
- Python 2
- Python 3
- Boto3 library
- Python 3 requests library

Various utility scripts specific to this project are also installed:
- create_captions.sh, the overall control script to manage the process
- transcribe_all_mp4_from_bucket.py, serially starts, monitors, and retrieves results for each transcription job
- convert_all_json_to_vtt.py, saves .json results from the transcription process to .vtt files
- webVttUtils.py, pared down version of AWS transcription utilities found  [here](https://github.com/aws-samples/aws-transcribe-captioning-tools/blob/master/tools/webvttUtils.py), to perform the json to vtt conversion

