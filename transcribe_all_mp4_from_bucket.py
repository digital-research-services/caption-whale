import os
import time
from uuid import uuid4
import boto3
import requests
import glob


transcribe = boto3.client('transcribe')

os.chdir("/incoming_mp4")
filelist = glob.glob("*.mp4") + glob.glob("*.m4v")

for mp4file in filelist:
    print(mp4file)
 
    job_name = mp4file.replace(" ", "_") + "_job_" + str(uuid4())
    job_uri = "https://vtwxvideocaptionuploads.s3.amazonaws.com/" + mp4file 
    transcribe.start_transcription_job(
        TranscriptionJobName=job_name,
        Media={'MediaFileUri': job_uri},
        MediaFormat='mp4',
        LanguageCode='en-US'
    )
    while True:
        status = transcribe.get_transcription_job(TranscriptionJobName=job_name)
        if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
            break
    
        print(f'processing {job_name}')
        time.sleep(30)

    print(status)
    json_transcription_location = status['TranscriptionJob']['Transcript']['TranscriptFileUri']
    print(json_transcription_location)
    json_transcription = requests.get(json_transcription_location, allow_redirects=True)

    open(mp4file + ".transcript.json" , 'wb').write(json_transcription.content)

