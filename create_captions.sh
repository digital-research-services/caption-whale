#!/usr/bin/bash

if [ ! -d /incoming_mp4 ]; then
    echo "Please specify an MP4_DIR as per the instructions in README.md"
    exit 1
fi

# Upload mp4 files from volume bind_mounted as mp4 to S3 bucket 
cd /incoming_mp4
aws s3 cp . s3://vtwxvideocaptionuploads/ --recursive --exclude "*" --include "*.mp4"

# Upload m4v files from volume bind_mounted as mp4 to S3 bucket
aws s3 cp . s3://vtwxvideocaptionuploads/ --recursive --exclude "*" --include "*.m4v"

# Submit transcription jobs to AWS
python3 /transcribe_all_mp4_from_bucket.py

# Convert .json files to .vtt files
python /convert_all_json_to_vtt.py /incoming_mp4

# Delete the mp4 files that we uploaded in this session from
# the bucket. Leave them in the local directory.  
for file in *.mp4
do
    aws s3 rm "s3://vtwxvideocaptionuploads/$file"
done

# Delete m4v files from the bucket
for file in *.m4v
do
    aws s3 rm "s3://vtwxvideocaptionuploads/$file"
done
