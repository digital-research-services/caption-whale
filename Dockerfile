FROM amazonlinux:2

# Install Python 3 and libraries
RUN yum install -y python3
RUN curl -O https://bootstrap.pypa.io/get-pip.py \
    && python3 get-pip.py \
    && rm get-pip.py
RUN pip install boto3
RUN pip install requests

# Install and configure AWS CLI
ADD aws_credentials /root/.aws/credentials
ADD aws_config /root/.aws/config
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && yum install -y unzip \
    && unzip awscliv2.zip \
    && yum remove -y unzip \
    && rm awscliv2.zip \
    && ./aws/install \
    && rm -rf aws

# Add project scripts
ADD create_captions.sh /
ADD transcribe_all_mp4_from_bucket.py /
ADD convert_all_json_to_vtt.py /
ADD webvttUtils.py /

CMD bash /create_captions.sh
