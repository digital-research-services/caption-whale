#convert_all_json_to_vtt.py
import sys
import glob
import os

from webvttUtils import writeTranscriptToWebVTT 

input_directory = sys.argv[1]

os.chdir(input_directory)
for jsonfile in glob.glob("*.mp4.transcript.json"):
    vttfilename = jsonfile.replace("mp4.transcript.json", "vtt")
    print(vttfilename)
    with open(jsonfile, "r") as f:
        data = writeTranscriptToWebVTT(f.read(), 'en', vttfilename)

for jsonfile in glob.glob("*.m4v.transcript.json"):
    vttfilename = jsonfile.replace("m4v.transcript.json", "vtt")
    print(vttfilename)
    with open(jsonfile, "r") as f:
        data = writeTranscriptToWebVTT(f.read(), 'en', vttfilename)
